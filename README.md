# Pathology Information System - Mobile application
=========================================================

The new CIP Pathology Laboratory Information Management System is developed by [CIP](https://cipotato.org/). The mobile application is a key tool for recording test results and uploading images from the Pathology laboratory of the International Potato Center (CIP).

This platform is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 [License](http://www.gnu.org/licenses/)

This project began in 2017 with funds from the Genebank platform of [Crop Trust](https://www.croptrust.org/) within the framework of a five-year project for the empowerment of the CGIAR Germplasm Health Units and its development was in charge of the Informatics Unit for the Research (RIU) of CIP and consists of the development of a new platform for the management and as a repository of data generated by the Pathology laboratory as a result of the diagnostic services it provides to its internal users, which are the Genebank and the Breeding Program. This platform is made up of 2 applications (APPs), a Web and a Mobile, and it is expected that it will replace the old application called CIPVIR, which is no longer supported due to its age and development format.

| Support | Person | Email |
| ------ | ------ | ------ |
| Pathology Laboratory Head | Giovanna Muller | g.muller@cgiar.org |
| Pathology Laboratory Coordinator | Nataly Franco | n.e.franco@cgiar.org |
| IT Support | Edwin Rojas | e.rojas@cgiar.org |

Related IT project: [Web application](https://gitlab.com/CIP-Development/pathology-system-web/-/blob/main/README.md)

Related IT project: [Webservice](https://gitlab.com/CIP-Development/pathology-system-webservice/-/blob/main/README.md)

<details><summary>IT Specifications (click to expand)</summary>


| Technology | Specification |
| ------ | ------ |
| Language | [Kotlin](https://developer.android.com/kotlin?gclid=Cj0KCQjwtrSLBhCLARIsACh6Rmh4SRUy_GwnmCv0fiE_Yy4-jt2sYVt0E3Ef4AGpCHyK2HtpZG_VxXQaAlbGEALw_wcB&gclsrc=aw.ds) 1.3.21 |
| Database engine | MySQL Light with [Room Database 2.1.0](https://google-developer-training.github.io/android-developer-fundamentals-course-concepts-v2/unit-4-saving-user-data/lesson-10-storing-data-with-room/10-1-c-room-livedata-viewmodel/10-1-c-room-livedata-viewmodel.htmlurl) |
| IDE | [Android Studio 4.1](https://developer.android.com/studio) with [Gradle 3.1.0](https://gradle.org/) |
| Architecture pattern | [MVVM](https://developer.android.com/jetpack/guide) |
| Others | [Retrofit2 2.5.0](https://square.github.io/retrofit/), [Lifecycle 2.0.0](https://developer.android.com/guide/components/activities/activity-lifecycle), [Navigation 2.1.0](https://developer.android.com/jetpack/androidx/releases/navigation), [OkHttp3 3.14.1](https://square.github.io/okhttp/), [Work RunTime 2.0.1](https://developer.android.com/jetpack/androidx/releases/work), [Material Design 1.1.0](https://material.io/design) |

TabletPC with Android 5.1:
- Model: Zebra ET5X [View more](https://www.zebra.com/us/en/products/tablets/et5x-series.html) 
- Processor: 4 núcleos, 1.33 GHz, 2MB
- Memory RAM: 2GB
- Internal storage: 32GB
- External storage: 16GB
- Protocol WiFi: 802.11 ac



Relational Model Diagram:
![View Diagram](https://gitlab.com/CIP-Development/pathology-system-mobile/-/blob/main/documentation/RelationalMobileDiagram.png)

Database Data Dictionary - Mobile:
![View](https://gitlab.com/CIP-Development/pathology-system-mobile/-/blob/main/documentation/Data_Dictionary_Mobile.xlsx)

Use Case Diagram:
![View Diagram](https://gitlab.com/CIP-Development/pathology-system-mobile/-/blob/main/documentation/use_case_diagram.pdf)

Architectural Diagram:
![View Diagram](https://gitlab.com/CIP-Development/pathology-system-mobile/-/blob/main/documentation/architecture_diagram.png)

Navigation Diagram (xml file in documentation directory):
![View Diagram](https://gitlab.com/CIP-Development/pathology-system-mobile/-/blob/main/documentation/NavigationDiagram.jpeg)

Class Diagram:
![View Diagram](https://gitlab.com/CIP-Development/pathology-system-mobile/-/blob/main/documentation/Classes_Diagram.pdf)

Package Diagram:
![View Diagram](https://gitlab.com/CIP-Development/pathology-system-mobile/-/blob/main/documentation/package_diagram.png)



</details>


