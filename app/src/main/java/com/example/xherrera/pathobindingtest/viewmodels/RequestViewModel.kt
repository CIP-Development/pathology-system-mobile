package com.example.xherrera.pathobindingtest.viewmodels

import android.util.Log
import com.example.xherrera.pathobindingtest.App.Companion.isNetworkAvailable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.*
import com.example.xherrera.pathobindingtest.App
import com.example.xherrera.pathobindingtest.database.PathoDatabase
import com.example.xherrera.pathobindingtest.database.PathoRepo
import com.example.xherrera.pathobindingtest.database.entities.*
import com.example.xherrera.pathobindingtest.extDb.ApiService
import com.example.xherrera.pathobindingtest.workers.UpdateRequestListWorker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
class RequestViewModel: ViewModel() {

    private val apiServe by lazy {
        ApiService.create()
    }
    private val dBase = PathoDatabase.getInstance(App.appContext())
    private val repo = PathoRepo(apiServe, dBase)

    val requestListLiveData: MutableLiveData<List<Request>> = MutableLiveData()
    val requestProcesses: MutableLiveData<List<ReqProcess>> = MutableLiveData()
    val sampleListLiveData: MutableLiveData<List<Sample>> = MutableLiveData()
    val agentByProcessList: MutableLiveData<List<AgentByProcess>> = MutableLiveData()
    val activityList: MutableLiveData<List<Activity>> = MutableLiveData()

    private fun validateLocalData(requestId: Int, sampleQty: Int, agentQty: Int): Boolean{
        return repo.getLocalSamplesEager(requestId).count() == sampleQty &&
                repo.getLocalAgentsEager(requestId).count() == agentQty
    }

    fun deleteRequestData(requestId: Int){
        repo.deleteLocalRequestData(requestId)
    }

    fun deleteEssayData(requestId: Int, essayId: Int){
        repo.deleteLocalEssayData(requestId, essayId)
    }

    fun purgeLocalRequestData(requestId: Int, sampleQty: Int, agentQty: Int){
        if (!validateLocalData(requestId, sampleQty, agentQty)){
            deleteRequestData(requestId)
        }
    }

    fun getRequests(){
        viewModelScope.launch {
            if (isNetworkAvailable()){
                val requestList = repo.getRequests()
                requestListLiveData.postValue(requestList)
                repo.deleteLocalRequests()
                repo.insertRequests(requestList!!)
            } else{
                val requestList = repo.getLocalRequests()
                requestListLiveData.postValue(requestList)
            }
        }
    }

    fun getActivities(){
        viewModelScope.launch {
            if (isNetworkAvailable()){
                    val activities = repo.getActivities()
                    activityList.postValue(activities)
                    repo.deleteLocalActivities()
                    repo.insertActivities(activities)
                } else {
                    val activities = repo.getLocalActivities()
                    activityList.postValue(activities)
            }
        }
    }

    fun getAgentsByProcess(requestId: Int){
        viewModelScope.launch {
            if(repo.getLocalAgents(requestId).isNotEmpty()){
                val agentByProcess = repo.getLocalAgents(requestId)
                agentByProcessList.postValue(agentByProcess)
            } else if(isNetworkAvailable()){
                val agentByProcess = repo.getAgentsByProcess(requestId)
                agentByProcessList.postValue(agentByProcess)
                repo.insertAgents(agentByProcess!!)
            } else{
            }
        }
    }

    fun getRqDetails(requestId: Int, numOrderId: Int){
        viewModelScope.launch {
            if(repo.getLocalReqDetail(requestId).isNotEmpty()){
                val reqProcessList = repo.getLocalReqDetail(requestId)
                requestProcesses.postValue(reqProcessList)
            } else if(isNetworkAvailable()){
                val reqProcessList = repo.getRqDetails(requestId, numOrderId)
                requestProcesses.postValue(reqProcessList)
                repo.insertReqProcesses(reqProcessList!!)
            } else{
            }
        }
    }

    fun getSamples(requestId: Int, numOrderId: Int){
        viewModelScope.launch {
            if(repo.getLocalSamples(requestId, numOrderId).isNotEmpty()){
                val sampleList = repo.getLocalSamples(requestId, numOrderId)
                sampleListLiveData.postValue(sampleList)
            } else if(isNetworkAvailable()) {
                val sampleList = repo.getSamples(requestId, numOrderId)
                sampleListLiveData.postValue(sampleList)
                repo.insertSamples(sampleList!!)
            } else{
            }
        }
    }

    fun workUpdateRequestList(){
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val request = OneTimeWorkRequestBuilder<UpdateRequestListWorker>()
            .setConstraints(constraints)
            .addTag("update-rqlist")
            .setBackoffCriteria(BackoffPolicy.LINEAR, 60, TimeUnit.SECONDS)
            .build()

        WorkManager.getInstance()
            .beginUniqueWork("update-rqlist", ExistingWorkPolicy.KEEP, request)
            .enqueue()
    }

    init {
        Log.i("RequestViewModel", "RequestViewModel has been created !!!")
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("RequestViewModel", "RequestViewModel has been destroyed !!!")
    }
}