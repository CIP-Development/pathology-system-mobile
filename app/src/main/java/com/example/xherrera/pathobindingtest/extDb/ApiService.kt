package com.example.xherrera.pathobindingtest.extDb


import com.example.xherrera.pathobindingtest.database.entities.*
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface ApiService {

    @GET("ReadRequests.php")
    fun getRequestsAsync(): Deferred<List<Request>>

    @GET("ReadRequests.php")
    fun getRequests(): Call<List<Request>>

    @GET("GetRqReportList.php")
    fun getRqReports(): Call<ArrayList<RqReport>>

    @FormUrlEncoded
    @POST("GetSamples.php")
    fun getSamples(
        @Field("requestId") requestId: Int,
        @Field("numOrderId") numOrderId: Int
    ): Deferred<List<Sample>>

    @GET("GetActivities.php")
    fun getActivities(): Deferred<List<Activity>>

    @FormUrlEncoded
    @POST("GetAgentsByProcess.php")
    fun getAgentsByProcess(
        @Field("requestId") requestId: Int
    ): Deferred<List<AgentByProcess>>

    @GET("GetSymptoms.php")
    fun getSymptoms(): Deferred<List<Symptom>>

    @FormUrlEncoded
    @POST("GetSupports.php")
    fun getSupports(
        @Field("requestId") requestId: Int,
        @Field("essayId") essayId: Int
    ): Call<List<Support>>

    @FormUrlEncoded
    @POST("GetSlots.php")
    fun getSlots(
        @Field("requestId") requestId: Int,
        @Field("essayId") essayId: Int
    ): Call<List<PreSlot>>

    @FormUrlEncoded
    @POST("GetLecturesReport.php")
    fun getLecturesReport(
        @Field("requestId") requestId: Int,
        @Field("essayId") essayId: Int,
        @Field("requestId") agentId: Int
    ): Call<ArrayList<Lecture>>

    @FormUrlEncoded
    @POST("ReadReqDetails.php")
    fun getReqDetails(
        @Field("requestId") requestId: Int,
        @Field("numOrderId") numOrderId: Int
    ): Deferred<ArrayList<ReqProcess>>

    @POST("PostRDArray.php")
    fun postRduArray(@Body suppList: List<ReadingDataUnit>): Call<ArrayList<Response>>

    @POST("PostSuppArray.php")
    fun postSuppArray(@Body suppList: List<Support>): Call<ArrayList<Response>>

    @POST("UpdateRDArray.php")
    fun updateRduArray(@Body suppList: List<ReadingDataUnit>): Call<ArrayList<Response>>

    @FormUrlEncoded
    @POST("PostTokenSample.php")
    fun postTokenSample(
        @Field("requestId") requestId: Int,
        @Field("cropId") cropId: Int,
        @Field("workflowId") workflowId: Int,
        @Field("numOrderId") numOrderId: Int
    ): Call<Response>

    @FormUrlEncoded
    @POST("Login.php")
    fun tryLogin(
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<Response>

    companion object {
        fun create(): ApiService{

            val client = OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            client.addInterceptor(loggingInterceptor)

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://52.30.61.201/restapi/") //DELETE "DEV" FOR PRODUCTION
                .client(client.build())
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}