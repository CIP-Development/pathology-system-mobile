package com.example.xherrera.pathobindingtest.database.entities

import androidx.room.Entity
import com.google.gson.annotations.Expose

@Entity(primaryKeys = ["plantId", "sampleId", "plantPosition", "essayId", "requestId", "rduType", "symptomId", "activityId"])
data class PlantSlot(
    @Expose val plantId: Int, //agentId
    @Expose val sampleId: Int,
    @Expose val plantPosition: Int, //cellPosition
    @Expose var result: String,
    @Expose val rduType: Int,
    @Expose val activityId: Int,
    @Expose val requestId: Int,
    @Expose val essayId: Int,
    @Expose val cropId: Int?,
    @Expose val numOrderId: Int,
    @Expose val workflowId: Int,
    @Expose val symptomId: Int,
    @Expose val symptomName: String,
    @Expose val agentId: Int
)