package com.example.xherrera.pathobindingtest.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Slot
import kotlinx.android.synthetic.main.cust_child_item.view.*

class ChildCustomAdapter(
    private val slotList: ArrayList<Slot>,
    private val listener: Listener
):RecyclerView.Adapter<ChildCustomAdapter.ViewHolder>() {

    interface Listener{
        fun onItemClick(slot: Slot, position: Int)
    }

    private val colors: Array<String> = arrayOf(
        "#D1C4E9",
        "#C8E6C9",
        "#80D8FF",
        "#FFCDD2",
        "#CCFF90",
        "#B2DFDB",
        "#A7FFEB",
        "#C5CAE9",
        "#FF9E80",
        "#E1BEE7",
        "#80DEEA",
        "#69F0AE",
        "#EA80FC"
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cust_child_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(slotList[position], listener, colors)
    }

    override fun getItemCount(): Int {
        return slotList.count()
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bind(
            slot: Slot,
            listener: Listener,
            colors: Array<String>
        ){
            itemView.setOnClickListener { listener.onItemClick(slot, adapterPosition) }
            itemView.tvCustCell.text = slot.cellPosition.toString()
            when {
                slot.type == "first" -> itemView.cvCustChild.setCardBackgroundColor(Color.parseColor("#EF5350"))
                slot.type == "empty" -> itemView.cvCustChild.setCardBackgroundColor(Color.WHITE)
                slot.type == "blind" -> itemView.cvCustChild.setCardBackgroundColor(Color.parseColor("#9E9E9E"))
                slot.type == "control" -> itemView.cvCustChild.setCardBackgroundColor(Color.parseColor("#FFF59D"))
                else -> itemView.cvCustChild.setCardBackgroundColor(Color.parseColor(colors[slot.agentId%13]))
            }
        }
    }
}