package com.example.xherrera.pathobindingtest.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.PathoCheckAdapter
import com.example.xherrera.pathobindingtest.database.entities.AgentByProcess
import com.example.xherrera.pathobindingtest.database.entities.Sample
import com.example.xherrera.pathobindingtest.viewmodels.EssayViewModel
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.dialog_pathogens.view.*
import kotlinx.android.synthetic.main.fragment_pcr_gel_setup.view.*


class PcrGelSetupFragment: Fragment(), PathoCheckAdapter.Listener {

    private lateinit var pcrGelSetupView: View
    private lateinit var essayViewModel: EssayViewModel
    private lateinit var etSampleQty: TextInputEditText
    private lateinit var etPathogenQty: TextInputEditText //TODO Change for CheckBoxGroup
    private lateinit var etControlQty: TextInputEditText
    private lateinit var etLadderQty: TextInputEditText
    private lateinit var etGelSize: TextInputEditText
    private lateinit var btnPatho: MaterialButton
    private lateinit var btnProceed: MaterialButton
    private lateinit var tvGelInfo: TextView
    private lateinit var rvPathoCheck: RecyclerView

    private var requestId: Int = 0
    private var essayId: Int = 0
    private var supportId: Int = 0
    private var gelQty: Int = 1
    private lateinit var requestCode: String
    private var numOrderId: Int? = 0
    private var activityId: Int = 0

    private lateinit var sampList: ArrayList<Sample>
    private lateinit var pathoList: ArrayList<AgentByProcess>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        pcrGelSetupView = inflater.inflate(R.layout.fragment_pcr_gel_setup, container, false)
        essayViewModel = ViewModelProviders.of(this).get(EssayViewModel::class.java)

        etSampleQty = pcrGelSetupView.insSampleQty
        etPathogenQty = pcrGelSetupView.insPathoQty
        etControlQty = pcrGelSetupView.insCtrlQty
        etLadderQty = pcrGelSetupView.insLadderQty
        etGelSize = pcrGelSetupView.insgelSize
        tvGelInfo = pcrGelSetupView.tvRqInfo
        btnPatho = pcrGelSetupView.btnPatho
        btnProceed = pcrGelSetupView.btnProceed
        pathoList = arrayListOf()

        val args = PcrGelSetupFragmentArgs.fromBundle(arguments!!)
        requestId = args.rqId
        requestCode = args.rqCode
        essayId = args.essayId
        supportId = args.gelId
        numOrderId = args.numOrderId
        activityId = args.activityId
        gelQty = args.gelQty

        essayViewModel.getRequestData(requestId, essayId, numOrderId!!)
        tvGelInfo.text = "Request: $requestCode | Gel: $supportId"

        setBtnPathoListener()
        setBtnProceedListener()

        return pcrGelSetupView
    }

    private fun setBtnPathoListener(){
        btnPatho.setOnClickListener {
            val dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_pathogens, null)
            val builder = androidx.appcompat.app.AlertDialog.Builder(activity!!)
                .setView(dialogView)
                .setTitle("Select pathogens for gel $supportId")
                .setPositiveButton("OK") { _,_ ->
                    println(pathoList)
                }
                .setNegativeButton("Cancel", null)

            val dialog = builder.create()
            dialog.show()

            rvPathoCheck = dialogView.rv_patho_check
            val agentList = essayViewModel.essayAgents.value

            rvPathoCheck.layoutManager = LinearLayoutManager(activity)
            rvPathoCheck.setItemViewCacheSize(100)
            rvPathoCheck.adapter = PathoCheckAdapter(agentList!!, this)
            rvPathoCheck.itemAnimator = DefaultItemAnimator()
        }
    }

    private fun setBtnProceedListener(){
        btnProceed.setOnClickListener {
            val sampleQty = etSampleQty.text.toString().toInt()
            val controlQty = etControlQty.text.toString().toInt()
            val ladderQty = etLadderQty.text.toString().toInt()
            val gelSize = etGelSize.text.toString().toInt()

            if (gelSize < (sampleQty+controlQty+ladderQty)){
                Toast.makeText(this.context, "Check the total quantity of slots", Toast.LENGTH_LONG)
                    .show()
            } else {
                println("Activity ID: $activityId")
                val pcrGelBrief = essayViewModel.createPcrGelBrief(
                    activityId, supportId, controlQty, ladderQty, sampleQty
                )
                val pcrSlots = essayViewModel.newGenPcrSlots(pcrGelBrief, pathoList, gelQty)

                AlertDialog.Builder(activity)
                    .setTitle("${pcrSlots.count()} PCR slots generated")
                    .setPositiveButton("OK") { _,_ ->
                        essayViewModel.updateSupport(requestId, essayId, activityId, supportId, sampleQty, controlQty)
                        findNavController().navigate(PcrGelSetupFragmentDirections
                            .actionPcrGelSetupFragmentToPcrGelResultFragment(
                                requestId, requestCode, essayId, supportId, numOrderId?:1, activityId
                            ))
                    }
                    .create().show()
            }
        }
    }

    override fun onItemAdd(pathogen: AgentByProcess, position: Int) {
        pathoList.add(pathogen)
        println(pathoList)
    }

    override fun onItemQuit(pathogen: AgentByProcess, position: Int) {
        pathoList.remove(pathogen)
        println(pathoList)
    }
}