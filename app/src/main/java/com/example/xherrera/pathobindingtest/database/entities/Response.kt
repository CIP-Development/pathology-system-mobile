package com.example.xherrera.pathobindingtest.database.entities

import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("status") val status: Int,
    @SerializedName("status_message") val statusMessage: String
)