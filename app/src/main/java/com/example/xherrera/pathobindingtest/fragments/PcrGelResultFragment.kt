package com.example.xherrera.pathobindingtest.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.PcrResultParentAdapter
import com.example.xherrera.pathobindingtest.database.entities.ReadingDataUnit
import com.example.xherrera.pathobindingtest.database.entities.Slot
import com.example.xherrera.pathobindingtest.viewmodels.EssayViewModel
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.fragment_pcr_gel_result.view.*


class PcrGelResultFragment: Fragment() {

    private lateinit var resultView: View
    private lateinit var essayViewModel: EssayViewModel
    private var supportId: Int = 0
    private var requestId: Int = 0
    private var essayId: Int = 0
    private var numOrderId: Int = 0
    private var activityId: Int = 0
    private lateinit var requestCode: String
    private lateinit var sampleList: ArrayList<Int>
    private lateinit var slotList: ArrayList<Slot>
    private lateinit var saveSlotList: ArrayList<Slot>

    private lateinit var rvPcrResult: RecyclerView
    private lateinit var rvAdapter: RecyclerView.Adapter<PcrResultParentAdapter.ViewHolder>
    private lateinit var rvManager: LinearLayoutManager
    private lateinit var rvAnimator: RecyclerView.ItemAnimator
    private lateinit var tvGelInfo: TextView
    private lateinit var btnUpload: MaterialButton

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        resultView = inflater.inflate(R.layout.fragment_pcr_gel_result, container, false)
        essayViewModel = ViewModelProviders.of(this).get(EssayViewModel::class.java)
        
        val args = PcrGelResultFragmentArgs.fromBundle(arguments!!)
        supportId = args.supportId
        requestId = args.rqId
        essayId = args.essayId
        numOrderId = args.numOrderId
        requestCode = args.rqCode
        activityId = args.activityId

        essayViewModel.getRequestData(requestId, essayId, numOrderId)
        tvGelInfo = resultView.tv_support_info
        tvGelInfo.text = "Request: $requestCode | Gel: $supportId"

        //TODO Check RecyclerView Setup
        essayViewModel.essaySlots.observe(this, Observer {
            println(it.first())
            println(it.last())
            println("Observed Slots: ${it.count()}")
            slotList = it.filter { slot -> slot.supportId == supportId
                    && slot.activityId == activityId } as ArrayList<Slot>
            println("Filtered slots: ${slotList.count()}")
            sampleList = arrayListOf()
            slotList.distinctBy{ item -> item.cellPosition }.forEach { slot ->
                sampleList.add(slot.labProcessId)
            }
            println("Passed Slot List: ${sampleList.count()}")
            setRvAdapter(slotList, sampleList)
        })


        //TODO Setup btnUpload onCLickListener to actually upload results
        setBtnUploadListener()

        return resultView
    }

    private fun setRvAdapter(slotList: ArrayList<Slot>, sampleList: ArrayList<Int>){
        println("Slots at adapter: ${slotList.count()}")
        rvPcrResult = resultView.rv_gel_samples
        rvManager = LinearLayoutManager(this.context)
        rvAdapter = PcrResultParentAdapter(sampleList, slotList)
        rvAnimator = DefaultItemAnimator()

        rvPcrResult.layoutManager = rvManager
        rvPcrResult.setItemViewCacheSize(100)
        rvPcrResult.itemAnimator = rvAnimator
        rvPcrResult.adapter = rvAdapter
    }

    private fun setBtnUploadListener(){
        btnUpload = resultView.btnSendDistribution
        btnUpload.setOnClickListener {
            val rduList: ArrayList<ReadingDataUnit> = ArrayList()
            println("Slot list at method: ${slotList.count()}")
            essayViewModel.insertSlots(requestId, essayId, slotList)
            Toast.makeText(this.context, "Se subirán los resultados", Toast.LENGTH_LONG)
                .show()
            for(i in slotList){
                val rduType = when (i.type) {
                    "control" -> {11
                    }
                    "empty" -> {73
                    }
                    "ladder" -> {149
                    }
                    else -> 13
                }
                val rdu = ReadingDataUnit(
                    agentId = i.agentId,
                    sampleId = i.sampleId,
                    cellPosition = i.cellPosition,
                    result = i.result,
                    supportId = i.supportId,
                    activityId = i.activityId,
                    essayId = i.essayId,
                    requestId = requestId,
                    cropId = i.cropId,
                    numOrderId = i.numOrderId,
                    workFlowId = i.workflowId,
                    readingDataTypeId = rduType,
                    symptomId = 1
                )
                rduList.add(rdu)
            }
            println("RDUs: ${rduList.count()}")
            //Saving into the Room Database
            essayViewModel.insertRdus(requestId, essayId, rduList)
            //Posting through the API rest
            essayViewModel.workPostRduList(requestId, essayId, activityId, (slotList.first().cropId), slotList.first().workflowId, numOrderId)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        findNavController().navigate(PcrGelResultFragmentDirections
            .actionPcrGelResultFragmentToPcrGelListFragment(
                essayViewModel.essayAgents.value!!.count(), //TODO Validate this parameter assignation
                requestId,
                requestCode,
                essayId,
                numOrderId
            ))
        return true
    }
}