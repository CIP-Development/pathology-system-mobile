package com.example.xherrera.pathobindingtest.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.xherrera.pathobindingtest.database.entities.Support

@Dao
interface SupportDao {

    @Query("SELECT * FROM Support")
    fun getSupportsAsync(): LiveData<List<Support>>

    @Query("SELECT * FROM Support")
    fun getSupports(): List<Support>

    @Query("SELECT * FROM Support WHERE requestId = :requestId AND essayId = :essayId AND activityId = :activityId")
    fun getSpecificSupports(requestId: Int, essayId: Int, activityId: Int): List<Support>

    @Query("SELECT * FROM Support WHERE requestId = :requestId AND essayId = :essayId")
    fun getEssaySupports(requestId: Int, essayId: Int): List<Support>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(support: Support)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(support: Support)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(supportList: List<Support>)

    @Query("UPDATE Support SET usedSlots = :usedSlots, ctrlQty = :ctrlSlots WHERE requestId = :requestId AND essayId = :essayId AND activityId = :activityId AND supportId = :supportId")
    fun updateSupport(supportId: Int, requestId: Int, essayId: Int, activityId: Int, usedSlots: Int, ctrlSlots: Int)

    @Query("DELETE FROM Support WHERE requestId = :requestId")
    fun deleteSupports(requestId: Int)

    @Query("DELETE FROM Support WHERE requestId = :requestId AND essayId = :essayId")
    fun deleteSupportsByEssay(requestId: Int, essayId: Int)
}