package com.example.xherrera.pathobindingtest.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.xherrera.pathobindingtest.database.PathoDatabase
import com.example.xherrera.pathobindingtest.extDb.ApiService

class PostSupporListWorker(context: Context, params: WorkerParameters): Worker(context, params) {

    private val dBase by lazy {
        PathoDatabase.getInstance(context)
    }
    private val apiServe by lazy {
        ApiService.create()
    }

    override fun doWork(): Result {
        makeStatusNotification("The supports were sent", applicationContext)

        val requestId = inputData.getInt("rqId", 1)
        val essayId = inputData.getInt("essId", 1)
        val activityId = inputData.getInt("actId", 1)

        val localSupportList = if (essayId == 1 || essayId == 4 || essayId == 5 || essayId == 12){
            dBase.supportDao().getSpecificSupports(requestId, essayId, activityId)
        } else {
            dBase.supportDao().getEssaySupports(requestId, essayId)
        }
        println("Local Supports: ${localSupportList.count()}")
        println(localSupportList.last())

        val response = apiServe.postSuppArray(localSupportList).execute()

        if (response.isSuccessful){
            return Result.success()
        } else{
            if (response.code() in 500..599){
                return Result.retry()
            }
            return Result.failure()
        }
    }
}