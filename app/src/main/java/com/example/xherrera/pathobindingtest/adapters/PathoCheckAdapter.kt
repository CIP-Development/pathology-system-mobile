package com.example.xherrera.pathobindingtest.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.AgentByProcess
import kotlinx.android.synthetic.main.item_patho_check.view.*

class PathoCheckAdapter(
    private val pathoList: List<AgentByProcess>,
    private val listener: Listener
): RecyclerView.Adapter<PathoCheckAdapter.ViewHolder>() {
    interface Listener{
        fun onItemAdd(pathogen: AgentByProcess, position: Int)
        fun onItemQuit(pathogen: AgentByProcess, position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_patho_check, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(pathoList[position], listener, position)
    }

    override fun getItemCount(): Int = pathoList.count()

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bind(
            pathogen: AgentByProcess,
            listener: Listener,
            position: Int
        ){
            val pathoCheck = itemView.cb_patho
            itemView.cb_patho.text = pathogen.agentName
            pathoCheck.setOnClickListener {
                if (pathoCheck.isChecked) listener.onItemAdd(pathogen, position)
                if (!pathoCheck.isChecked) listener.onItemQuit(pathogen, position)
            }
        }
    }
}