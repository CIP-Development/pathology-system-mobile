package com.example.xherrera.pathobindingtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.xherrera.pathobindingtest.databinding.ActivityEssayBinding
import kotlinx.android.synthetic.main.fragment_supports_setup.*

class EssayActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityEssayBinding>(this, R.layout.activity_essay)

    }
}
