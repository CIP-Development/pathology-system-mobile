package com.example.xherrera.pathobindingtest.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Slot
import kotlinx.android.synthetic.main.cell_item.view.*
import kotlinx.android.synthetic.main.item_pcr_child_list.view.*
import kotlinx.android.synthetic.main.item_pcr_child_list.view.cvCell
import kotlinx.android.synthetic.main.item_pcr_child_list.view.imgDiagnosis
import kotlinx.android.synthetic.main.item_pcr_child_list.view.tvCell

class PcrResultChildAdapter(
    private val slotList: ArrayList<Slot>,
    private val listener: Listener
): RecyclerView.Adapter<PcrResultChildAdapter.ViewHolder>() {

    interface Listener {
        fun onItemClick(slot: Slot)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pcr_child_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(slotList[position], listener)
    }

    override fun getItemCount(): Int {
        return slotList.count()
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bind(slot: Slot, listener: Listener){
            itemView.tvCell.text = "${slot.labProcessId}-${slot.agentName}"
            itemView.setOnClickListener { listener.onItemClick(slot) }

            when (slot.type) {
                "ladder" -> {
                    itemView.cvCell.setCardBackgroundColor(Color.parseColor("#AD1457"))
                    itemView.tvCell.setTextColor(Color.WHITE)
                }
                "control" -> {
                    itemView.cvCell.setCardBackgroundColor(Color.parseColor("#FFF59D"))
                }
            }
            when (slot.result) {
                "positive" -> itemView.imgDiagnosis.setImageResource(R.drawable.ic_positive)
                "negative" -> itemView.imgDiagnosis.setImageResource(R.drawable.ic_negative)
                "doubt" -> itemView.imgDiagnosis.setImageResource(R.drawable.ic_doubt)
                "buffer" -> itemView.imgDiagnosis.setImageResource(R.drawable.ic_buffer_prev)
                "ladder" -> itemView.imgDiagnosis.setImageResource(R.drawable.ic_ladder_prev)
            }
        }
    }
}