package com.example.xherrera.pathobindingtest.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.xherrera.pathobindingtest.database.entities.Sample

@Dao
interface SampleDao {

    @Query("SELECT * FROM Sample WHERE requestId = :requestId")
    fun getSamples(requestId: Int): List<Sample>

    @Query("SELECT * FROM Sample")
    suspend fun getSampleList(): List<Sample>

    @Query("SELECT * FROM Sample WHERE requestId = :requestId AND numOrderId = :numOrderId")
    suspend fun getSampWhere(requestId: Int, numOrderId: Int): List<Sample>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(sample: Sample)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(sample: Sample)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(sampleList: List<Sample>)

    @Query("DELETE FROM Sample WHERE requestId = :requestId")
    fun deleteSamples(requestId: Int)

}