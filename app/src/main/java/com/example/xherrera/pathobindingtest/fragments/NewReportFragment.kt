package com.example.xherrera.pathobindingtest.fragments


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.LecturesListAdapter
import com.example.xherrera.pathobindingtest.databinding.FragmentNewReportBinding
import com.example.xherrera.pathobindingtest.viewmodels.ReportDetailViewModel
import com.example.xherrera.pathobindingtest.viewmodels.ReportDetailViewModelFactory
import kotlinx.android.synthetic.main.fragment_new_report.view.*

class NewReportFragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val args = NewReportFragmentArgs.fromBundle(this.arguments!!)
        val binding = DataBindingUtil.inflate<FragmentNewReportBinding>(inflater, R.layout.fragment_new_report, container, false)

        binding.root.tvRqId.text = "Request: ${args.requestId}"
        binding.root.tvEssayName.text = "Essay: ${args.essayName}"
        binding.root.tvAgentName.text = "Agent: ${args.agentName}"

        val reportDetailViewModel = ViewModelProviders
            .of(this, ReportDetailViewModelFactory(args.requestId, args.essayId, args.agentId))
            .get(ReportDetailViewModel::class.java)
        val resultList = reportDetailViewModel.resultList.value!!

        val recyclerView = binding.root.rvLectures
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = LecturesListAdapter(resultList)

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
}
