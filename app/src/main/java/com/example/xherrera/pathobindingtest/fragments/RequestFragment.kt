package com.example.xherrera.pathobindingtest.fragments


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.RqListAdapter
import com.example.xherrera.pathobindingtest.databinding.FragmentRequestBinding
import com.example.xherrera.pathobindingtest.database.entities.Request
import com.example.xherrera.pathobindingtest.viewmodels.RequestViewModel
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
class RequestFragment : Fragment(), RqListAdapter.Listener {

    private var manager: RecyclerView.LayoutManager ?= null
    private var disposable: Disposable?= null

    @SuppressLint("CheckResult")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentRequestBinding>(
            inflater,
            R.layout.fragment_request,
            container,
            false
        )

        val recyclerView = binding.rvRequests
        manager = LinearLayoutManager(activity!!)
        recyclerView.layoutManager = manager
        recyclerView.itemAnimator = DefaultItemAnimator()

        val requestViewModel = ViewModelProviders.of(this).get(RequestViewModel::class.java)
        requestViewModel.getRequests()
        requestViewModel.workUpdateRequestList()

        requestViewModel.requestListLiveData.observe(this, Observer {
            recyclerView.adapter = RqListAdapter(it, this)
        })

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        findNavController().navigate(RequestFragmentDirections.actionRequestFragmentToHomeFragment())
        return true
    }

    override fun onItemClick(request: Request){

        return findNavController().navigate(RequestFragmentDirections.actionRequestFragmentToRqDetailFragment(
            request.id,
            request.cropName,
            request.pathogenQty!!,
            request.sampleQty!!,
            request.creationDate!!,
            request.code,
            request.details!!,
            request.numOrderId!!
        ))
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }
}
