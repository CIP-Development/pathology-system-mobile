package com.example.xherrera.pathobindingtest.database.dao

import androidx.room.*
import com.example.xherrera.pathobindingtest.database.entities.PlantSlot

@Dao
interface PlantSlotDao {

    @Query("SELECT * FROM PlantSlot WHERE requestId = :requestId AND essayId = :essayId")
    suspend fun getEssayPlantRdus(requestId: Int, essayId: Int): List<PlantSlot>

    @Query("SELECT * FROM PlantSlot WHERE plantId = :plantId AND requestId = :requestId AND essayId = :essayId AND activityId = :activityId")
    suspend fun getPlantRduByPlant(plantId: Int, requestId: Int, essayId: Int, activityId: Int): List<PlantSlot>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(plantSlot: PlantSlot)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(plantSlotList: List<PlantSlot>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(plantSlot: PlantSlot)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateAll(plantSlotList: List<PlantSlot>)

    @Query("DELETE FROM PlantSlot WHERE requestId = :requestId")
    fun deletePlantSlots(requestId: Int)

    @Query("DELETE FROM PlantSlot WHERE requestId = :requestId AND essayId = :essayId")
    fun deletePlantSlotsByEssay(requestId: Int, essayId: Int)
}