package com.example.xherrera.pathobindingtest.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Support
import kotlinx.android.synthetic.main.item_gel_list.view.*
import kotlinx.android.synthetic.main.rqlist_item.view.*

class PcrGelListAdapter(
    private val supportList: List<Support>,
    private val listener: Listener
): RecyclerView.Adapter<PcrGelListAdapter.ViewHolder>() {

    interface Listener {
        fun onItemClick(support: Support)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(supportList[position], listener, position)
    }

    override fun getItemCount(): Int = supportList.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_gel_list, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        fun bind(
            support: Support,
            listener: Listener,
            pos: Int
        ){
            val supportId = support.supportId
            val sampleQty = support.usedSlots
            val controlQty = support.ctrlQty

            itemView.setOnClickListener { listener.onItemClick(support) }
            itemView.tv_gel_number.text = "Gel Number: $supportId"
            itemView.tv_gel_samples.text = "Samples: $sampleQty"
            itemView.tv_gel_controls.text = "Controls: $controlQty"
        }
    }

}