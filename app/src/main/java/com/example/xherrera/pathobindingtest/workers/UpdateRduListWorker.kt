package com.example.xherrera.pathobindingtest.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.xherrera.pathobindingtest.database.PathoDatabase
import com.example.xherrera.pathobindingtest.extDb.ApiService

class UpdateRduListWorker(context: Context, params: WorkerParameters): Worker(context, params) {

    private val dBase by lazy {
        PathoDatabase.getInstance(context)
    }
    private val apiServe by lazy {
        ApiService.create()
    }

    override fun doWork(): Result {
        makeStatusNotification("The results were updated", applicationContext)

        val requestId = inputData.getInt("rqId", 1)
        val essayId = inputData.getInt("essId", 1)
        val activityId = inputData.getInt("actId", 1)
        val numOrderId = inputData.getInt("numOrderId", 1)

        val localRduList = dBase.readingDataUnitDao().getRequestSpecRdus(requestId, essayId, activityId, numOrderId)

        val response = apiServe.updateRduArray(localRduList).execute()

        if (response.isSuccessful){
            return Result.success()
        } else{
            if (response.code() in 500..599){
                return Result.retry()
            }
            return Result.failure()
        }
    }
}