package com.example.xherrera.pathobindingtest.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.RqReport
import kotlinx.android.synthetic.main.rqlist_item.view.*

class ReportListAdapter(
    private val reportList: ArrayList<RqReport>,
    private val listener: Listener
): RecyclerView.Adapter<ReportListAdapter.ViewHolder>() {

    interface Listener {
        fun onItemClick(rqReport: RqReport)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(reportList[position], listener, position)
    }

    override fun getItemCount(): Int = reportList.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rqlist_item, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bind(
            rqReport: RqReport,
            listener: Listener,
            pos: Int
        ){
            itemView.setOnClickListener { listener.onItemClick(rqReport) }
            itemView.tvRqCode.text = "Request: ${rqReport.requestCode}"
            itemView.tvSampleQty.text = "Lectures: ${rqReport.rduQty}"
            itemView.tvRqCropName.text = "Essay: ${rqReport.essayName}"
            itemView.tvPathoQty.text = "Agent: ${rqReport.agentName}"
            if(pos%2 == 0){
                itemView.rvRequestItem.setBackgroundColor(Color.parseColor("#eeeeee"))
            }else{itemView.rvRequestItem.setBackgroundColor(Color.WHITE)}
        }
    }
}