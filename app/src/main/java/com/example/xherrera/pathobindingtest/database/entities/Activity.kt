package com.example.xherrera.pathobindingtest.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["activityId", "essayId"])
data class Activity(
    @Expose @SerializedName("activityId") @ColumnInfo(name = "activityId") val activityId: Int,
    @Expose @SerializedName("activityName") @ColumnInfo(name = "activityName") val activityName: String,
    @Expose @SerializedName("essayId") @ColumnInfo(name = "essayId") val essayId: Int
)