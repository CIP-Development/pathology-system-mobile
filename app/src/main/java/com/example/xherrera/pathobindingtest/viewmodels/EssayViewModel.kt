package com.example.xherrera.pathobindingtest.viewmodels

import android.util.Log
import androidx.lifecycle.*
import androidx.work.*
import com.example.xherrera.pathobindingtest.App
import com.example.xherrera.pathobindingtest.database.PathoDatabase
import com.example.xherrera.pathobindingtest.database.PathoRepo
import com.example.xherrera.pathobindingtest.database.entities.*
import com.example.xherrera.pathobindingtest.extDb.ApiService
import com.example.xherrera.pathobindingtest.lab.EssayBrief
import com.example.xherrera.pathobindingtest.workers.PostRduListWorker
import com.example.xherrera.pathobindingtest.workers.PostSupporListWorker
import com.example.xherrera.pathobindingtest.workers.PostTokenSampleWorker
import com.example.xherrera.pathobindingtest.workers.UpdateRduListWorker
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt
import kotlin.random.Random
import com.example.xherrera.pathobindingtest.lab.LabDistribution
import com.example.xherrera.pathobindingtest.lab.PcrGelBrief

class EssayViewModel: ViewModel(), LabDistribution {

    private val apiServe by lazy{
        ApiService.create()
    }
    private val dBase = PathoDatabase.getInstance(App.appContext())
    private val repo = PathoRepo(apiServe, dBase)

    val essayAgents: MutableLiveData<List<AgentByProcess>> = MutableLiveData()
    val essaySamples: MutableLiveData<MutableList<Sample>> = MutableLiveData()
    val essaySupports: MutableLiveData<List<Support>> = MutableLiveData()
    val essaySlots: MutableLiveData<List<Slot>> = MutableLiveData()
    val essayPack: MutableLiveData<EssayPack> = MutableLiveData()
    val essayRdus: MutableLiveData<List<ReadingDataUnit>> = MutableLiveData()
    val essayActivities: MutableLiveData<List<Activity>> = MutableLiveData()

    val requestId: MutableLiveData<Int> = MutableLiveData()
    val essayId: MutableLiveData<Int> = MutableLiveData()
    val workflowId: MutableLiveData<Int> = MutableLiveData()
    val cropId: MutableLiveData<Int> = MutableLiveData()
    val numOrderId: MutableLiveData<Int> = MutableLiveData()

    init {
        Log.i("EssayViewModel", "EssayViewModel started")
    }

    fun getServerPack(requestId: Int, essayId: Int){
        viewModelScope.launch {
            withContext(Dispatchers.Default){
                val supportList = repo.getServerSupportsByEssay(requestId, essayId)
                val preSlotList = repo.getSlots(requestId, essayId)
                val slotList = mutableListOf<Slot>()
                preSlotList?.forEach {
                    val type = when (it.type) {
                        13 -> "sample"
                        73 -> "empty"
                        12 -> "blind"
                        11 -> "control"
                        139 -> "10%"
                        140 -> "problem"
                        149 -> "ladder"
                        else -> "dBlind"
                    }
                    val agentName = when (it.agentId) {
                        1 -> "PLRV"
                        5 -> "PSTVd"
                        6 -> "PVY"
                        7 -> "PVA"
                        8 -> "PVX"
                        9 -> "PVS"
                        12 -> "PMTV"
                        15 -> "PVT"
                        19 -> "PYV"
                        21 -> "SPFMV"
                        22 -> "SPMMV"
                        23 -> "SPCV"
                        24 -> "SPCFV"
                        26 -> "C-6"
                        27 -> "SPMSV"
                        28 -> "SPCSV"
                        43 -> "SPLV"
                        110 -> "SPVG"
                        112 -> "Begomovirus"
                        114 -> "phytoplasm "
                        133 -> "SPLCV"
                        134 -> "SPVCV"
                        136 -> "Geminivirus"
                        137 -> "SPV2"
                        139 -> "SPVC"
                        else -> "Agent"
                    }
                    val slot = Slot(
                        labProcessId = it.labProcessId,
                        agentId = it.agentId,
                        sampleId = it.sampleId,
                        supportId = it.supportId,
                        requestId = it.requestId,
                        essayId = it.essayId,
                        agentName = agentName,
                        cellPosition = it.cellPosition,
                        result = it.result,
                        type = type,
                        cropId = it.cropId,
                        workflowId = it.workFlowId,
                        activityId = it.activityId,
                        numOrderId = it.numOrderId
                    )
                    slotList.add(slot)
                }
                if (!supportList.isNullOrEmpty()){
                    essaySupports.postValue(supportList)
                    repo.insertSupports(supportList)
                }
                if (!slotList.isNullOrEmpty()){
                    essaySlots.postValue(slotList)
                    repo.insertSlots(slotList)
                }
                println("SUPPORTS: $supportList")
                println("SLOTS: $slotList")
                println("AGENTS: ${essayAgents.value}")
            }
        }
    }

    fun getRequestData(reqId: Int, essId: Int, numOrder: Int){
        viewModelScope.launch {
            val reqProcess = repo.getLocalSpecificReqDetail(reqId, essId)
            val agentList = repo.getLocalAgentsByEssay(reqId, essId)
            val sampleList = repo.getLocalSamples(reqId, numOrder)
            val supportList = repo.getSupportsByEssay(reqId, essId)
            val slotList = repo.getSlotsByEssay(reqId, essId)
            val activityList = repo.getLocalActivitiesByEssay(essId)
            essaySamples.postValue(sampleList as MutableList<Sample>)
            essayAgents.postValue(agentList)
            essayActivities.postValue(activityList)
            requestId.postValue(reqProcess.requestId)
            essayId.postValue(reqProcess.essayId)
            workflowId.postValue(reqProcess.workFlowId)
            essaySupports.postValue(supportList)
            essaySlots.postValue(slotList)
            cropId.postValue(reqProcess.cropId)
            numOrderId.postValue(numOrder)
        }
    }

    fun fillEssayPack(pack: EssayPack){
        viewModelScope.launch{
            essayPack.postValue(pack)
            repo.insertSupports(pack.essSupports)
            repo.insertSlots(pack.essSlots)
            essaySupports.postValue(pack.essSupports)
            essaySlots.postValue(pack.essSlots)
        }
    }

    fun getEssayPack(requestId: Int, essayId: Int, numOrderId: Int){
        viewModelScope.launch {
            val slots = repo.getSlotsByEssay(requestId, essayId)
            essaySlots.postValue(null)                          //-------------------- TAKE CARE WITH THIS ONE!!
            essaySlots.postValue(slots)
            val supports = repo.getSupportsByEssay(requestId, essayId)
            essaySupports.postValue(supports)
            val agents = repo.getLocalAgentsByEssay(requestId, essayId)
            essayAgents.postValue(agents)
            val samples = repo.getLocalSamples(requestId, numOrderId)
            essaySamples.postValue(samples as MutableList<Sample>)
            val activityList = repo.getLocalActivitiesByEssay(essayId)
            essayActivities.postValue(activityList)
            val pack = EssayPack(samples, agents, supports, slots)
            println("PACK SUPPORTS: ${pack.essSupports.count()}")
            println("PACK SLOTS: ${pack.essSlots.count()}")
            essayPack.postValue(pack)
        }
    }

    fun insertRdus(requestId: Int, essayId: Int, rduList: ArrayList<ReadingDataUnit>){
        viewModelScope.launch {

            if(repo.getSpecificRdus(requestId, essayId, rduList.first().activityId, rduList.first().numOrderId, rduList.first().supportId).isNullOrEmpty()){
            //if(repo.getLocalRdusByEssay(requestId, essayId).isNullOrEmpty()){
                println("RDUs passed: ${rduList.count()}")
                repo.insertRdus(rduList)
                essayRdus.postValue(rduList as List<ReadingDataUnit>)
            }
            else{
                repo.updateRdus(rduList)
                essayRdus.postValue(repo.getLocalRdusByEssay(requestId, essayId))
            }
        }
    }

    private fun insertSupports(requestId: Int, essayId: Int, supportList: List<Support>){
        viewModelScope.launch {
            if (repo.getSupportsByEssay(requestId, essayId).isNullOrEmpty()){
                println("Supports passed: ${supportList.count()}")
                repo.insertSupports(supportList)
                essaySupports.postValue(supportList)
            } else{
                essaySupports.postValue(repo.getSupportsByEssay(requestId, essayId))
            }
        }
    }

    fun updateSlots(slotList: List<Slot>){
        viewModelScope.launch {
            repo.updateSlots(slotList)
            essaySlots.postValue(slotList)
        }
    }

    fun insertSlots(requestId: Int, essayId: Int, slotList: List<Slot>){
        viewModelScope.launch {
            if(repo.getSlotsByEssay(requestId, essayId).filter { it.supportId == slotList.first().supportId && it.activityId == slotList.first().activityId }.isNullOrEmpty()) {
                println("Slots passed: ${slotList.count()}")
                repo.insertSlots(slotList)
                essaySlots.postValue(slotList)
            } else{
                essaySlots.postValue(repo.getSlotsByEssay(requestId, essayId))
            }
        }
    }

    fun workUpdateRduList(rqId: Int, essId: Int, actId: Int, numOrderId: Int){

        val data = Data.Builder()
        data.putInt("rqId", rqId)
        data.putInt("essId", essId)
        data.putInt("actId", actId)
        data.putInt("numOrderId", numOrderId)

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val request = OneTimeWorkRequestBuilder<UpdateRduListWorker>()
            //.setConstraints(constraints)
            .setInputData(data.build())
            .addTag("update-rdulist")
            .setBackoffCriteria(BackoffPolicy.LINEAR, 30, TimeUnit.SECONDS)
            .build()

        WorkManager.getInstance()
            .beginUniqueWork("update-rdulist", ExistingWorkPolicy.KEEP, request)
            .enqueue()

    }

    fun workPostRduList(rqId: Int, essId: Int, actId: Int, cropId: Int, workflowId: Int, numOrderId: Int){

        val data = Data.Builder()
        data.putInt("rqId", rqId)
        data.putInt("essId", essId)
        data.putInt("actId", actId)
        data.putInt("cropId", cropId)
        data.putInt("workflowId", workflowId)
        data.putInt("numOrderId", numOrderId)

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val preRequest = OneTimeWorkRequestBuilder<PostSupporListWorker>()
            .setConstraints(constraints)
            .setInputData(data.build())
            .addTag("post-supplist")
            .setBackoffCriteria(BackoffPolicy.LINEAR, 30, TimeUnit.SECONDS)
            .build()

        val putToken = OneTimeWorkRequestBuilder<PostTokenSampleWorker>()
            //.setConstraints(constraints)
            .setInputData(data.build())
            .addTag("post-token")
            .setBackoffCriteria(BackoffPolicy.LINEAR, 30, TimeUnit.SECONDS)
            .build()

        val request = OneTimeWorkRequestBuilder<PostRduListWorker>()
            .setConstraints(constraints)
            .setInputData(data.build())
            .addTag("post-rdulist")
            .setBackoffCriteria(BackoffPolicy.LINEAR, 30, TimeUnit.SECONDS)
            .build()

        WorkManager.getInstance()
            .beginWith(preRequest)
            .then(putToken)
            .then(request)
            .enqueue()
    }

    /**
     * USING THE LAB DISTRIBUTION LIBRARY FUNCTIONS
     */
    fun createEssayBrief(
        blindQty: Int,
        doubleBlindQty: Int,
        withTenPercent: Boolean
    ): EssayBrief{
        return EssayBrief(
            requestId = requestId.value!!,
            essayId = essayId.value!!,
            numOrderId = numOrderId.value!!,
            cropId = cropId.value!!,
            workflowId = workflowId.value!!,
            pathogenQty = essayAgents.value!!.size,
            sampleQty = essaySamples.value!!.size,
            blindQty = blindQty,
            dBlindQty = doubleBlindQty,
            withTenPercent = withTenPercent,
            activityQty = essayActivities.value!!.size
            )
    }

    fun createPcrEssayBrief(
        supportQty: Int
    ): EssayBrief{
        return EssayBrief(
            requestId = requestId.value!!,
            essayId = essayId.value!!,
            numOrderId = numOrderId.value!!,
            cropId = cropId.value!!,
            workflowId = workflowId.value!!,
            pathogenQty = essayAgents.value!!.size,
            sampleQty = essaySamples.value!!.size,
            supportByPathogen = supportQty,
            blindQty = 0,
            dBlindQty = 0,
            withTenPercent = false,
            activityQty = essayActivities.value!!.size
        )
    }

    fun createPcrGelBrief(
        activityId: Int,
        supportId: Int,
        controlQty: Int,
        ladderQty: Int,
        sampleQty: Int
    ): PcrGelBrief{
        return PcrGelBrief(
            requestId = requestId.value!!,
            essayId = essayId.value!!,
            numOrderId = numOrderId.value!!,
            cropId = cropId.value!!,
            workflowId = workflowId.value!!,
            activityId = activityId,
            supportId = supportId,
            controlQty = controlQty,
            ladderQty = ladderQty,
            sampleQty = sampleQty
        )
    }

    fun updateSupport(requestId: Int, essayId: Int, activityId: Int, supportId: Int, usedSlots: Int, ctrlSlots: Int){
        repo.updateSupport(requestId, essayId, activityId, supportId, usedSlots, ctrlSlots)
    }

    fun newGenPcrSupports(
        essayBrief: EssayBrief
    ): List<Support>{
        val activityList = essayActivities.value!!

        val supportList = genPcrSupports(essayBrief, activityList)
        insertSupports(requestId.value!!, essayId.value!!, supportList)

        return supportList
    }

    fun newGenPcrSlots(
        pcrGelBrief: PcrGelBrief,
        pathoList: List<AgentByProcess>,
        getlQty: Int
    ): List<Slot>{
        val supports = essaySupports.value!!.filter { it.activityId == pcrGelBrief.activityId }
        val supportId = pcrGelBrief.supportId
        val sampleList = if(supportId == 1){
            essaySamples.value!!.filter { it.numOrder!! <= pcrGelBrief.sampleQty }
        } else {
            var usedSlots = 0
            var auxSupportId = supportId
            while ((auxSupportId - 2) >= 0){
                usedSlots += supports[supportId - 2].usedSlots
                auxSupportId -= 1
                println("Used slots: $usedSlots")
            }

            essaySamples.value!!.filter { (it.numOrder!! > usedSlots) && (it.numOrder!! <= (usedSlots + pcrGelBrief.sampleQty)) }
        }
        /*else{
            essaySamples.value!!.filter { it.sampleId > essaySlots.value!!.last { slot -> slot.sampleId != 999 }.sampleId }
        }*/

        //val agentList = essayAgents.value!!.filter { it.essayId == essayId.value!! }

        val slotList = genPcrSlots(sampleList, pathoList, pcrGelBrief)
        insertSlots(requestId.value!!, essayId.value!!, slotList)

        println("Support ID: $supportId")
        println("First slot generated: ${slotList.first()}")
        println("Last slot generated: ${slotList.last()}")
        println("slotList: ${slotList.count()}")
        println("Inserted Slot List: ${essaySlots.value!!.count()}")
        return slotList
    }

    fun newGenEssayPack(
        essayBrief: EssayBrief,
        newControlBySupport: Int,
        controlLastSupport: Int,
        emptySlots: Int=0
    ): EssayPack{
        val agentList = essayAgents.value!!
        val sampleList = essaySamples.value!!
        val activityList = essayActivities.value!!
        val updatedEssayBrief = updateEssayBrief(essayBrief, newControlBySupport, controlLastSupport, emptySlots)
        val essayDistributor = genEssayDistributor(updatedEssayBrief)

        val supportList = genSupports(updatedEssayBrief, agentList, activityList)
        insertSupports(requestId.value!!, essayId.value!!, supportList)

        val slotList = genSlots(updatedEssayBrief, essayDistributor, agentList, sampleList, activityList)
        insertSlots(requestId.value!!, essayId.value!!, slotList)

        return EssayPack(sampleList, agentList, supportList, slotList)
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("EssayViewModel", "EssayViewModel has been destroyed !!!")
    }
}