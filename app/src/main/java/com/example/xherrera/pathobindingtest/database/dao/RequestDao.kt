package com.example.xherrera.pathobindingtest.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.xherrera.pathobindingtest.database.entities.Request

@Dao
interface RequestDao {

    @Query("SELECT * FROM Request")
    fun getRequests(): LiveData<List<Request>>

    @Query("SELECT * FROM Request ORDER BY id DESC")
    fun getRequestList(): List<Request>

    @Query("SELECT * FROM Request ORDER BY id DESC")
    suspend fun getRequestListAsync(): List<Request>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(request: Request)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(requestList: Request)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllAsync(requestList: List<Request>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(requestList: List<Request>)

    @Query("DELETE FROM Request")
    fun deleteAll()

}