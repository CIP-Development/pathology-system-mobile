package com.example.xherrera.pathobindingtest.database

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.xherrera.pathobindingtest.database.dao.*
import com.example.xherrera.pathobindingtest.database.entities.*

@Database(
    entities = [
        Request::class,
        Sample::class,
        ReqProcess::class,
        AgentByProcess::class,
        Support::class,
        Slot::class,
        ReadingDataUnit::class,
        Symptom::class,
        PlantBySample::class,
        PlantSlot::class,
        Activity::class
    ],
    version = 53,
    exportSchema = false
)
abstract class PathoDatabase: RoomDatabase() {

    abstract fun requestDao(): RequestDao
    abstract fun sampleDao(): SampleDao
    abstract fun reqProcessDao(): ReqProcessDao
    abstract fun agentByProcessDao(): AgentByProcessDao
    abstract fun supportDao(): SupportDao
    abstract fun slotDao(): SlotDao
    abstract fun readingDataUnitDao(): ReadingDataUnitDao
    abstract fun symptomDao(): SymptomDao
    abstract fun plantBySampleDao(): PlantBySampleDao
    abstract fun plantSlotDao(): PlantSlotDao
    abstract fun activityDao(): ActivityDao

    companion object {

        @Volatile
        private var INSTANCE: PathoDatabase? = null

        fun getInstance(
            context: Context
        ): PathoDatabase {

            return INSTANCE?: synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PathoDatabase::class.java,
                    "pathology"
                )
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .addCallback(PathoDBCallback())
                    .build()
                INSTANCE = instance
                instance
            }
        }

        private class PathoDBCallback:RoomDatabase.Callback(){


            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                Log.i("Database", "Database created")
                INSTANCE?.let { database ->
                    {
                        populateSymptoms(database.symptomDao())
                    }
                }
            }
        }

        fun populateSymptoms(symptomDao: SymptomDao) {
            var symp = Symptom(12,"B","Blistering")
            symptomDao.insert(symp)
            symp = Symptom(13,"NA","NA")
            symptomDao.insert(symp)
            symp = Symptom(14,"C","Chlorosis")
            symptomDao.insert(symp)
            symp = Symptom(15,"Cr","Chlorosis rings")
            symptomDao.insert(symp)
            symp = Symptom(16,"Cs","Chlorosis spots")
            symptomDao.insert(symp)
            symp = Symptom(17,"D","Dwarfing")
            symptomDao.insert(symp)
            symp = Symptom(18,"IVC","Intervenal chlorosis")
            symptomDao.insert(symp)
            symp = Symptom(19,"Ld","Leaf deformation")
            symptomDao.insert(symp)
            symp = Symptom(20,"Ln","Leaf necrotic")
            symptomDao.insert(symp)
            symp = Symptom(21,"Lr","Leaf reduction")
            symptomDao.insert(symp)
            symp = Symptom(22,"M","Mosaic")
            symptomDao.insert(symp)
            symp = Symptom(23,"Mo","Mottling")
            symptomDao.insert(symp)
            symp = Symptom(24,"Net","Nettling")
            symptomDao.insert(symp)
            symp = Symptom(25,"Np","Necrotic point")
            symptomDao.insert(symp)
            symp = Symptom(26,"R","Rugosity")
            symptomDao.insert(symp)
            symp = Symptom(27,"RD","Roll down")
            symptomDao.insert(symp)
            symp = Symptom(28,"RU","Roll up")
            symptomDao.insert(symp)
            symp = Symptom(29,"Vb","Vein banding")
            symptomDao.insert(symp)
            symp = Symptom(30,"Vc","Vein clearing")
            symptomDao.insert(symp)
            symp = Symptom(31,"Vn","Vein necrosis")
            symptomDao.insert(symp)
            symp = Symptom(32,"ALD","Apical leaf deformation")
            symptomDao.insert(symp)
            symp = Symptom(33,"AN","Apical necrosis")
            symptomDao.insert(symp)
            symp = Symptom(34,"APC","Apical purple color")
            symptomDao.insert(symp)
            symp = Symptom(35,"Crik","Crinkle")
            symptomDao.insert(symp)
            symp = Symptom(36,"Iys","Intervenal yellow spots")
            symptomDao.insert(symp)
            symp = Symptom(37,"Led","Leaf drop")
            symptomDao.insert(symp)
            symp = Symptom(38,"Lil","Little leaf")
            symptomDao.insert(symp)
            symp = Symptom(39,"LL","Local necrotic lesions")
            symptomDao.insert(symp)
            symp = Symptom(40,"LL ©","Chlorotic local lesions")
            symptomDao.insert(symp)
            symp = Symptom(41,"LR (Lef roll of lower leaves)","Leaf roll of lower leaves")
            symptomDao.insert(symp)
            symp = Symptom(42,"M (m)","Mild Mosaic")
            symptomDao.insert(symp)
            symp = Symptom(43,"M (Sev)","Severe Mosaic")
            symptomDao.insert(symp)
            symp = Symptom(44,"NLP","Necrotic line patterns")
            symptomDao.insert(symp)
            symp = Symptom(45,"Nr","Necrotic ring")
            symptomDao.insert(symp)
            symp = Symptom(46,"Ns","Necrotic spots")
            symptomDao.insert(symp)
            symp = Symptom(47,"Urg","Up right growing")
            symptomDao.insert(symp)
            symp = Symptom(48,"Vy","Vein yellowing")
            symptomDao.insert(symp)
        }
    }
}



