package com.example.xherrera.pathobindingtest.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class ReportDetailViewModelFactory(
    private val requestId: Int,
    private val essayId: Int,
    private val agentId: Int
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ReportDetailViewModel(requestId, essayId, agentId) as T
    }
}