package com.example.xherrera.pathobindingtest.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.PlantSlot
import com.example.xherrera.pathobindingtest.database.entities.Symptom
import kotlinx.android.synthetic.main.symptom_check_item.view.*

class SymptomCheckAdapter(
    private val plantSlotList: List<PlantSlot>,
    private val listener: Listener
): RecyclerView.Adapter<SymptomCheckAdapter.ViewHolder>() {

    interface Listener {
        fun onItemClick(plantSlot: PlantSlot, position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.symptom_check_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(plantSlotList[position], listener, position)
    }

    override fun getItemCount(): Int = plantSlotList.count()

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bind(
            plantSlot: PlantSlot,
            listener: Listener,
            position: Int
        ){
            itemView.symptomCheck.setOnClickListener { listener.onItemClick(plantSlot, position) }
            itemView.tvSymptomName.text = "${plantSlot.symptomName}"
            when (plantSlot.result) {
                "positive" -> {
                    itemView.tvSymptomName.setTextColor(Color.parseColor("#4f9a94"))
                    itemView.symptomCheck.setImageResource(R.drawable.ic_checkbox_checked)
                }
                "doubt" -> {
                    itemView.tvSymptomName.setTextColor(Color.parseColor("#4f9a94"))
                    itemView.symptomCheck.setImageResource(R.drawable.ic_checkbox_undeterminated)
                }
                "negative" -> {
                    itemView.symptomCheck.setImageResource(R.drawable.ic_checkbox_blank)
                }
            }
        }
    }
}