package com.example.xherrera.pathobindingtest.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.xherrera.pathobindingtest.database.entities.ReadingDataUnit

@Dao
interface ReadingDataUnitDao {

    @Query("SELECT * FROM ReadingDataUnit")
    fun getRdusAsync(): LiveData<List<ReadingDataUnit>>

    @Query("SELECT * FROM ReadingDataUnit")
    fun getRdus(): List<ReadingDataUnit>

    @Query("SELECT * FROM ReadingDataUnit WHERE requestId = :requestId AND essayId = :essayId AND activityId = :activityId AND numOrderId = :numOrderId")
    fun getRequestSpecRdus(requestId: Int, essayId: Int, activityId: Int, numOrderId: Int): List<ReadingDataUnit>

    @Query("SELECT * FROM ReadingDataUnit WHERE requestId = :requestId AND essayId = :essayId AND activityId = :activityId AND numOrderId = :numOrderId AND supportId = :supportId")
    suspend fun getSpecificRdusBySupport(requestId: Int, essayId: Int, activityId: Int, numOrderId: Int, supportId: Int): List<ReadingDataUnit>

    @Query("SELECT * FROM ReadingDataUnit WHERE requestId = :requestId AND essayId = :essayId")
    suspend fun getEssayRdus(requestId: Int, essayId: Int): List<ReadingDataUnit>

    @Query("SELECT * FROM ReadingDataUnit WHERE requestId = :requestId AND essayId = :essayId AND activityId = :activityId")
    suspend fun getSpecificRdus(requestId: Int, essayId: Int, activityId: Int): List<ReadingDataUnit>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(readingDataUnit: ReadingDataUnit)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateAll(rduList: List<ReadingDataUnit>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(readingDataUnit: ReadingDataUnit)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(rduList: List<ReadingDataUnit>)

    @Query("DELETE FROM ReadingDataUnit WHERE requestId = :requestId")
    fun deleteRdus(requestId: Int)

    @Query("DELETE FROM ReadingDataUnit WHERE requestId = :requestId AND essayId = :essayId")
    fun deleteRdusByEssay(requestId: Int, essayId: Int)
}