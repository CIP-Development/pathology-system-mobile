package com.example.xherrera.pathobindingtest.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity
data class Request(
    @SerializedName("requestId") @Expose @PrimaryKey val id:Int,
    @SerializedName("code") @Expose @ColumnInfo(name = "code") val code: String,
    @SerializedName("cropName") @Expose @ColumnInfo(name = "cropName") val cropName:String,
    @SerializedName("details") @Expose @ColumnInfo(name = "details") val details:String?,
    @SerializedName("pathogenQty") @Expose @ColumnInfo(name = "pathogenQty") val pathogenQty:Int?,
    @SerializedName("sampleQty") @Expose @ColumnInfo(name = "sampleQty") val sampleQty:Int?,
    @SerializedName("requiredDate") @Expose @ColumnInfo(name = "requiredDate") val requiredDate:String?,
    @SerializedName("numOrderId") @Expose @ColumnInfo(name = "numOrderId") val numOrderId: Int?,
    @SerializedName("creationDate") @Expose @ColumnInfo(name = "creationDate") val creationDate: String?,
    @SerializedName("essayDetail") @Expose @ColumnInfo(name = "essayDetail") val essayDetail: String?,
    @SerializedName("sampleType") @Expose @ColumnInfo(name = "sampleType") val sampleType: String?
)