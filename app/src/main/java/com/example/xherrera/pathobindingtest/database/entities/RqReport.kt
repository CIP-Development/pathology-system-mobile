package com.example.xherrera.pathobindingtest.database.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RqReport(
    @SerializedName("requestId") @Expose val requestId: Int,
    @SerializedName("requestCode") @Expose val requestCode: String,
    @SerializedName("rduQty") @Expose val rduQty: Int,
    @SerializedName("essayName") @Expose val essayName: String,
    @SerializedName("agentName") @Expose val agentName: String,
    @SerializedName("essayId") @Expose val essayId: Int,
    @SerializedName("requestId") @Expose val agentId: Int
)