package com.example.xherrera.pathobindingtest.fragments


import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.*
import com.example.xherrera.pathobindingtest.databinding.FragmentSupportsSetupBinding
import com.example.xherrera.pathobindingtest.viewmodels.EssayViewModel
import com.example.xherrera.pathobindingtest.viewmodels.RequestViewModel
import kotlinx.android.synthetic.main.fragment_supports_setup.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class SuppSetupFragment : Fragment() {

    private lateinit var sampList: ArrayList<Sample>
    private lateinit var agentList: ArrayList<AgentByProcess>
    private var reqTotSup: Int? = 0
    private var numOrderId: Int? = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.i("SuppSetupFragment", "SuppSetupFragment attached")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentSupportsSetupBinding>(
            inflater,
            R.layout.fragment_supports_setup,
            container,
            false
        )

        val args = SuppSetupFragmentArgs.fromBundle(arguments!!)
        val requestId = args.rqId
        val essayId = args.essayId
        val rqCode = args.rqCode
        numOrderId = args.numOrderId

        binding.root.tvRqInfo.text = "Request $rqCode"

        var addTenPercent = binding.checkTenPercent.isActivated

        binding.checkTenPercent.setOnCheckedChangeListener{ button, isChecked ->
            if (isChecked) {
                addTenPercent = true
                println("Add Ten Percent status : $addTenPercent")
            } else {
                addTenPercent = false
                println("Add Ten Percent status : $addTenPercent")
            }
        }


        val requestViewModel = ViewModelProviders.of(this).get(RequestViewModel::class.java)

        requestViewModel.sampleListLiveData.observe(this, Observer{
            sampList = it as ArrayList<Sample>
        })
        requestViewModel.agentByProcessList.observe(this, Observer { agents ->
            agentList = agents.filter { it.essayId == essayId } as ArrayList<AgentByProcess>
        })

        val essayViewModel = ViewModelProviders.of(this).get(EssayViewModel::class.java)
        essayViewModel.getRequestData(requestId, essayId, numOrderId!!)
        println("Activities: ${essayViewModel.essayActivities.value}")

        essayViewModel.essaySamples.observe(this, Observer {
            binding.insSampleQty.setText(it.count().toString())
        })
        essayViewModel.essayAgents.observe(this, Observer{
            binding.insPathoQty.setText(it.count().toString())
        })

        binding.btnProceed.setOnClickListener { v: View ->

            val pathoQty = binding.insPathoQty.text.toString().toInt()
            val blindQty = binding.insBlindQty.text.toString().toInt()
            val doubleBlindQty = binding.insDoubBlindQty.text.toString().toInt()
            val controlRegularSupport = binding.insCtrlQty.text.toString().toInt()
            val controlLastSupport = binding.insCtrlLastQty.text.toString().toInt()
            val emptySlots = binding.insUsedSlots.text.toString().toInt()
            val sampleQty = binding.insSampleQty.text.toString().toInt()
            var shareLastSupp = false

            val essayBrief = essayViewModel.createEssayBrief(blindQty, doubleBlindQty, addTenPercent)
            val pack = essayViewModel.newGenEssayPack(essayBrief, controlRegularSupport, controlLastSupport, emptySlots)
            essayViewModel.fillEssayPack(pack)

            reqTotSup = pack.essSupports.count()

            AlertDialog.Builder(activity)
                .setTitle("$reqTotSup plates generated")
                //.setMessage("")
                .setPositiveButton("OK") { _,_ ->
                    v.findNavController().navigate(SuppSetupFragmentDirections.
                    actionSupportsSetupFragmentToSupDashboardFragment(args.rqId, true, essayId, numOrderId!!, rqCode))
                }
                .create().show()
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
}
