package com.example.xherrera.pathobindingtest.lab

data class EssayBrief(
    val requestId: Int,
    val essayId: Int,
    val numOrderId: Int,
    val cropId: Int,
    val workflowId: Int,
    val slotBySupport: Int = 96,
    val pathogenQty: Int,
    val sampleQty: Int,
    val blindQty: Int,
    val dBlindQty: Int,
    val essayType: Int = essayId,
    val withTenPercent: Boolean,
    val activityQty: Int,
    var supportByPathogen: Int=0,
    var tenPercentSlots: Int=0,
    var controlByPathogen: Int=6,
    var problemByPathogen: Int=0,
    var rduByPathogen: Int=sampleQty+controlByPathogen,
    var controlBySupport: Int=6,
    var controlLastSupport: Int=6,
    var emptyPositions: Int=0
)