package com.example.xherrera.pathobindingtest.fragments


import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.adapters.SymptomsAdapter
import com.example.xherrera.pathobindingtest.database.entities.PlantSlot
import com.example.xherrera.pathobindingtest.database.entities.Symptom
import com.example.xherrera.pathobindingtest.databinding.FragmentPlantSympBinding
import com.example.xherrera.pathobindingtest.viewmodels.GreenHouseViewModel


class PlantSympFragment : Fragment(), SymptomsAdapter.Listener {

    private var recyclerView: RecyclerView? = null
    private val selectedSymptoms = mutableListOf<Symptom>()
    val REQUEST_IMAGE_CAPTURE = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        val binding = DataBindingUtil.inflate<FragmentPlantSympBinding>(inflater,
            R.layout.fragment_plant_symp, container, false)

        val args = PlantSympFragmentArgs.fromBundle(arguments!!)
        val plantId = args.plantId
        val requestId = args.requestId
        val essayId = args.essayId
        val activityId = args.activityId
        var sectionName = ""
        var plantPosition: Int? = null
        val sampleId = args.sampleId
        val cropId = args.cropId
        val workflowId = args.workflowId
        val numOrderId = args.numOrderId
        val agentId = args.agentId

        lateinit var dialog: AlertDialog
        val symptomArray = R.array.symptoms_short
        var plantSlotList = mutableListOf<PlantSlot>()

        val ghViewModel = ViewModelProviders.of(this).get(GreenHouseViewModel::class.java)
        ghViewModel.getBioPack(requestId, essayId, args.numOrderId)
        //ghViewModel.getPlantSlots(plantId, requestId, essayId, activityId)
        println("GH PLANT SLOTS ${ghViewModel.auxSlots}")

        val symptoms = ghViewModel.symptomList.sortedBy { it.longName }

        binding.tvRqInfo.text = "Request: $requestId    Essay: $essayId     Sample: $sampleId     Plant: $plantId     Activity: $activityId"

        /*
        ghViewModel.ghSlots.observe(this, Observer {
            plantSlotList = it.filter { plantSlot -> plantSlot.plantId == plantId }
        })
        */

        /**
         * REFINE THIS CODE !!!!!
         */
        /*
        if(!ghViewModel.auxSlots.isNullOrEmpty()){
            ghViewModel.auxSlots.forEach { plantSlot ->
                val auxSymptom = Symptom(
                    symptomId = plantSlot.symptomId,
                    shortName = plantSlot.result,
                    longName = symptoms.sortedBy { it.symptomId }[plantSlot.symptomId].longName,
                    plantPositionId = plantSlot.plantPosition
                )
                selectedSymptoms.add(auxSymptom)
            }
            println(selectedSymptoms)
        }
        */

        recyclerView = binding.rvSymptoms
        recyclerView!!.layoutManager = LinearLayoutManager(activity)
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = SymptomsAdapter(selectedSymptoms, this)

        binding.btnApical.setOnClickListener {
            sectionName = "APICAL"
            plantPosition = 1

            val builder = AlertDialog.Builder(activity!!, R.style.AlertDialogTheme)
            builder.setTitle("$sectionName section symptoms")
            builder.setIcon(R.drawable.ic_leaf_teal)
            builder.setMultiChoiceItems(symptomArray, null) { _, which, isChecked ->
                if (isChecked){
                    val auxSymptom = symptoms[which]
                    auxSymptom.plantPositionId = plantPosition
                    selectedSymptoms.add(auxSymptom)
                } else if (selectedSymptoms.contains(symptoms[which])){
                    selectedSymptoms.remove(symptoms[which])
                }
            }
            builder.setPositiveButton("OK"){_,_ ->
                println(selectedSymptoms)
                println(plantPosition.toString())
                recyclerView!!.adapter!!.notifyDataSetChanged()
            }
            dialog = builder.create()
            dialog.show()
        }
        binding.btnMedial.setOnClickListener {
            sectionName = "MEDIAL"
            plantPosition = 2

            val builder = AlertDialog.Builder(activity!!, R.style.AlertDialogTheme)
            builder.setTitle("$sectionName section symptoms")
            builder.setIcon(R.drawable.ic_leaf_teal)
            builder.setMultiChoiceItems(symptomArray, null) { _, which, isChecked ->
                if (isChecked){
                    val auxSymptom = symptoms[which]
                    auxSymptom.plantPositionId = plantPosition
                    selectedSymptoms.add(auxSymptom)
                } else if (selectedSymptoms.contains(symptoms[which])){
                    selectedSymptoms.removeAt(Integer.valueOf(which))
                }
            }
            builder.setPositiveButton("OK"){_,_ ->
                println(selectedSymptoms)
                println(plantPosition.toString())
                recyclerView!!.adapter!!.notifyDataSetChanged()
            }
            dialog = builder.create()
            dialog.show()
        }
        binding.btnBasal.setOnClickListener {
            sectionName = "BASAL"
            plantPosition = 3

            val builder = AlertDialog.Builder(activity!!, R.style.AlertDialogTheme)
            builder.setTitle("$sectionName section symptoms")
            builder.setIcon(R.drawable.ic_leaf_teal)
            builder.setMultiChoiceItems(symptomArray, null) { _, which, isChecked ->
                if (isChecked){
                    val auxSymptom = symptoms[which]
                    auxSymptom.plantPositionId = plantPosition
                    selectedSymptoms.add(auxSymptom)
                } else if (selectedSymptoms.contains(symptoms[which])){
                    selectedSymptoms.removeAt(Integer.valueOf(which))
                }
            }
            builder.setPositiveButton("OK"){_,_ ->
                println(selectedSymptoms)
                println(plantPosition.toString())
                recyclerView!!.adapter!!.notifyDataSetChanged()
            }
            dialog = builder.create()
            dialog.show()
        }

        binding.btnCamera.setOnClickListener {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
            //startActivity(intent)
        }

        binding.btnSave.setOnClickListener {

            selectedSymptoms.forEach {
                val plantSlot =  PlantSlot(
                    plantId = plantId,
                    plantPosition = it.plantPositionId!!,
                    sampleId = sampleId,
                    activityId = activityId,
                    essayId = essayId,
                    requestId = requestId,
                    result = it.shortName!!,
                    rduType = 13,
                    workflowId = workflowId,
                    numOrderId = numOrderId,
                    cropId = cropId,
                    symptomId = it.symptomId,
                    symptomName = it.longName!!,
                    agentId = agentId
                )
                plantSlotList.add(plantSlot)
            }
            ghViewModel.insertPlantSlots(requestId, essayId, plantSlotList)

            AlertDialog.Builder(activity!!)
                .setTitle("Local save")
                .setMessage("The results have been saved locally")
                .setPositiveButton("OK", null)
                .create().show()

        }

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onItemClick(symptom: Symptom) {
        selectedSymptoms.remove(symptom)
        recyclerView!!.adapter!!.notifyDataSetChanged()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
    }
}
