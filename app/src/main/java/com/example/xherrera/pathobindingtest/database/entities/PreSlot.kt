package com.example.xherrera.pathobindingtest.database.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PreSlot (
    @SerializedName("labProcessId") @Expose val labProcessId: Int,
    @SerializedName("agentId") @Expose val agentId: Int,
    @SerializedName("sampleId") @Expose val sampleId: Int,
    @SerializedName("supportId") @Expose val supportId: Int,
    @SerializedName("requestId") @Expose val requestId: Int,
    @SerializedName("essayId") @Expose val essayId: Int,
    @SerializedName("cellPosition") @Expose val cellPosition: Int,
    @SerializedName("result") @Expose val result: String,
    @SerializedName("type") @Expose val type: Int,
    @SerializedName("cropId") @Expose val cropId: Int,
    @SerializedName("workFlowId") @Expose val workFlowId: Int,
    @SerializedName("activityId") @Expose val activityId: Int,
    @SerializedName("numOrderId") @Expose val numOrderId: Int
)