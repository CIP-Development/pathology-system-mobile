package com.example.xherrera.pathobindingtest.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.ReqProcess
import kotlinx.android.synthetic.main.rq_process_item.view.*

class RqDetailAdapter(
    private val reqProcessList: List<ReqProcess>,
    private val listener: Listener
): RecyclerView.Adapter<RqDetailAdapter.ViewHolder>() {

    interface Listener{
        fun onItemClick(reqProcess: ReqProcess)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rq_process_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(reqProcessList[position], listener, position)
    }

    override fun getItemCount(): Int = reqProcessList.count()

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        fun bind(
            requestProcess: ReqProcess,
            listener: Listener,
            pos: Int
        ){
            itemView.setOnClickListener { listener.onItemClick((requestProcess)) }
            itemView.tvRqEssayName.text = requestProcess.essayName
            itemView.tvSampleQty.text = "Samples: ${requestProcess.sampleQty}"
            /*
            if (pos%2 == 0){
                itemView.cvReqProcess.setCardBackgroundColor(Color.parseColor("#eeeeee"))
            }else{itemView.cvReqProcess.setCardBackgroundColor(Color.WHITE)}
            */
        }
    }
}