package com.example.xherrera.pathobindingtest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.xherrera.pathobindingtest.databinding.ActivityLoginBinding
import com.example.xherrera.pathobindingtest.extDb.ApiService

class LoginActivity : AppCompatActivity() {

    private val apiServe by lazy{
        ApiService.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
        val btnSignIn = binding.emailSignInButton

        btnSignIn.setOnClickListener {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)

            val username = binding.username.text.toString()
            val password = binding.password.text.toString()

            val response = apiServe.tryLogin(
                username,
                password
            )

            response.execute()

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        }
    }
}
