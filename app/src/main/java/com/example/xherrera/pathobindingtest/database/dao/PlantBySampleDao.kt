package com.example.xherrera.pathobindingtest.database.dao

import androidx.room.*
import com.example.xherrera.pathobindingtest.database.entities.PlantBySample

@Dao
interface PlantBySampleDao {

    @Query("SELECT * FROM PlantBySample")
    fun getAllPlants(): List<PlantBySample>

    @Query("SELECT * FROM PlantBySample WHERE sampleId = :sampleId")
    suspend fun getPlantsBySample(sampleId: Int): List<PlantBySample>

    @Query("SELECT * FROM PlantBySample WHERE requestId = :requestId AND essayId = :essayId")
    suspend fun getEssayPlants(requestId: Int, essayId: Int): List<PlantBySample>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(plantBySample: PlantBySample)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(plantBySampleList: List<PlantBySample>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(plantBySample: PlantBySample)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateAll(plantBySampleList: List<PlantBySample>)

    @Query("DELETE FROM PlantBySample WHERE requestId = :requestId")
    fun deletePlants(requestId: Int)

    @Query("DELETE FROM PlantBySample WHERE requestId = :requestId AND essayId = :essayId")
    fun deletePlantsByEssay(requestId: Int, essayId: Int)
}