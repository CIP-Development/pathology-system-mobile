package com.example.xherrera.pathobindingtest.database.entities

data class EssayPack(
    val essSamples: MutableList<Sample>,
    val essAgents: List<AgentByProcess>,
    val essSupports: List<Support>,
    val essSlots: List<Slot>
)