package com.example.xherrera.pathobindingtest.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.*
import com.example.xherrera.pathobindingtest.database.PathoDatabase
import com.example.xherrera.pathobindingtest.extDb.ApiService
import com.example.xherrera.pathobindingtest.App
import com.example.xherrera.pathobindingtest.database.PathoRepo
import com.example.xherrera.pathobindingtest.database.entities.*
import com.example.xherrera.pathobindingtest.workers.PostRduListWorker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.anko.doAsync
import java.util.concurrent.TimeUnit

class GreenHouseViewModel: ViewModel() {

    private val apiServe by lazy {
        ApiService.create()
    }
    private val dBase = PathoDatabase.getInstance(App.appContext())
    private val repo = PathoRepo(apiServe, dBase)

    val symptomList = listOf(
        Symptom(12,"B","Blistering"),
        Symptom(13,"NA","NA"),
        Symptom(14,"C","Chlorosis"),
        Symptom(15,"Cr","Chlorosis rings"),
        Symptom(16,"Cs","Chlorosis spots"),
        Symptom(17,"D","Dwarfing"),
        Symptom(18,"IVC","Intervenal chlorosis"),
        Symptom(19,"Ld","Leaf deformation"),
        Symptom(20,"Ln","Leaf necrotic"),
        Symptom(21,"Lr","Leaf reduction"),
        Symptom(22,"M","Mosaic"),
        Symptom(23,"Mo","Mottling"),
        Symptom(24,"Net","Nettling"),
        Symptom(25,"Np","Necrotic point"),
        Symptom(26,"R","Rugosity"),
        Symptom(27,"RD","Roll down"),
        Symptom(28,"RU","Roll up"),
        Symptom(29,"Vb","Vein banding"),
        Symptom(30,"Vc","Vein clearing"),
        Symptom(31,"Vn","Vein necrosis"),
        Symptom(32,"ALD","Apical leaf deformation"),
        Symptom(33,"AN","Apical necrosis"),
        Symptom(34,"APC","Apical purple color"),
        Symptom(35,"Crik","Crinkle"),
        Symptom(36,"Iys","Intervenal yellow spots"),
        Symptom(37,"Led","Leaf drop"),
        Symptom(38,"Lil","Little leaf"),
        Symptom(39,"LL","Local necrotic lesions"),
        Symptom(40,"LL ©","Chlorotic local lesions"),
        Symptom(41,"LR","Leaf roll of lower leaves"),
        Symptom(42,"M (m)","Mild Mosaic"),
        Symptom(43,"M (Sev)","Severe Mosaic"),
        Symptom(44,"NLP","Necrotic line patterns"),
        Symptom(45,"Nr","Necrotic ring"),
        Symptom(46,"Ns","Necrotic spots"),
        Symptom(47,"Urg","Up right growing"),
        Symptom(48,"Vy","Vein yellowing")
    )

    val hostNames = listOf(
        "Mother Plant",
        "Gomphrena Globosa",
        "N. bigelovii x N clevelandii",
        "N. glutinosa",
        "N. Debneyii",
        "N. tabacum cv. white burley",
        "Physalis floridana",
        "C. murale",
        "N. benthamiana",
        "C. quinoa",
        "D. stramonium",
        "Lycopersicum esculetum 'Rutgers'",
        "Graft"
    )

    val ghSamples: MutableLiveData<List<Sample>> = MutableLiveData()
    val ghPlants: MutableLiveData<List<PlantBySample>> = MutableLiveData()
    val ghSlots: MutableLiveData<List<PlantSlot>> = MutableLiveData()
    val ghAgents: MutableLiveData<List<AgentByProcess>> = MutableLiveData()
    val ghBioPack: MutableLiveData<BioPack> = MutableLiveData()
    val ghSymptoms: MutableLiveData<List<Symptom>> = MutableLiveData()
    val ghActivities: MutableLiveData<List<Activity>> = MutableLiveData()
    val ghRdus: MutableLiveData<List<ReadingDataUnit>> = MutableLiveData()

    val requestId: MutableLiveData<Int> = MutableLiveData()
    val essayId: MutableLiveData<Int> = MutableLiveData()
    val workflowId: MutableLiveData<Int> = MutableLiveData()
    val cropId: MutableLiveData<Int> = MutableLiveData()
    val numOrderId: MutableLiveData<Int> = MutableLiveData()

    var auxSlots = listOf<PlantSlot>()

    init {
        Log.i("GreenHouseViewModel", "GreenHouseViewModel started")
        viewModelScope.launch {
            ghSymptoms.postValue(repo.getSymptomList())
        }
    }

    fun getPlantSlotsByPlant(plantId: Int, requestId: Int, essayId: Int, activityId: Int){
        viewModelScope.launch {
            withContext(Dispatchers.Main){
                ghSlots.postValue(repo.getSpecificPlantSlots(plantId, requestId, essayId, activityId))
                auxSlots = repo.getSpecificPlantSlots(plantId, requestId, essayId, activityId)
                println(repo.getSpecificPlantSlots(plantId, requestId, essayId, activityId))
                println(ghSlots.value)
                println("Aux Slots $auxSlots")
            }
        }
    }

    fun fillBioPack(bioPack: BioPack){
        println("Biopack PlantSlots: ${bioPack.bioEssPlantSlots.count()}")
        viewModelScope.launch {
            ghBioPack.postValue(bioPack)
            repo.insertPlants(bioPack.bioEssPlants)
            repo.insertPlantSlots(bioPack.bioEssPlantSlots)
            ghPlants.postValue(bioPack.bioEssPlants)
            ghSlots.postValue(bioPack.bioEssPlantSlots)
            println("SQLite PlantSlots: ${repo.getLocalPlantSlots(essayId.value!!, requestId.value!!).count()}")
            println(essayId.value!!)
            println(requestId.value!!)
        }
    }

    fun getRequestData(reqId: Int, essId: Int, numOrder: Int){
        viewModelScope.launch {
            val agentList = repo.getLocalAgentsByEssay(reqId, essId)
            val reqProcess = repo.getLocalSpecificReqDetail(reqId, essId)
            val plantList = repo.getLocalPlants(reqId, essId)
            val sampleList = repo.getLocalSamples(reqId, numOrder)
            val plantRduList = repo.getLocalPlantSlots(reqId, essId)
            val activityList = repo.getLocalActivitiesByEssay(essId)
            ghAgents.postValue(agentList)
            ghSamples.postValue(sampleList)
            ghPlants.postValue(plantList)
            ghSlots.postValue(plantRduList)
            ghActivities.postValue(activityList)
            requestId.postValue(reqId)
            essayId.postValue(essId)
            numOrderId.postValue(numOrder)
            workflowId.postValue(reqProcess.workFlowId)
            cropId.postValue(reqProcess.cropId)
        }
    }

    fun getBioPack(requestId: Int, essayId: Int, numOrderId: Int){
        viewModelScope.launch {
            val plantSlots = repo.getLocalPlantSlots(requestId, essayId)
            ghSlots.postValue(plantSlots)
            val plants = repo.getLocalPlants(requestId, essayId)
            ghPlants.postValue(plants)
            val samples = repo.getLocalSamples(requestId, numOrderId)
            ghSamples.postValue(samples)
            val symptoms = repo.getSymptomList()
            ghSymptoms.postValue(symptoms)
            val activityList = repo.getLocalActivitiesByEssay(essayId)
            ghActivities.postValue(activityList)
            val pack = BioPack(samples, plants, plantSlots)
            ghBioPack.postValue(pack)
            println("Plants at GetBioPack: ${plants.count()}")
            println("PlantSlots at GetBioPack: ${plantSlots.count()}")
        }
    }

    private fun insertPlants(requestId: Int, essayId: Int, plantList: List<PlantBySample>){
        println("Plants to be inserted: ${plantList.count()}")
        viewModelScope.launch {
            if(repo.getLocalPlants(requestId, essayId).isNullOrEmpty()){
                repo.insertPlants(plantList)
                ghPlants.postValue(plantList)
            } else {
                ghPlants.postValue(repo.getLocalPlants(requestId, essayId))
            }
        }
    }

    fun insertPlantSlots(requestId: Int, essayId: Int, plantSlotList: List<PlantSlot>){
        println("PlantSlots to be inserted: ${plantSlotList.count()}")
        viewModelScope.launch {
            //if(repo.getLocalPlantSlots(requestId, essayId).isNullOrEmpty()){
            repo.insertPlantSlots(plantSlotList)
            ghSlots.postValue(plantSlotList)
            /*} else{
                ghSlots.postValue(repo.getLocalPlantSlots(requestId, essayId))
            }*/
        }
    }

    fun insertRdus(requestId: Int, essayId: Int, rduList: ArrayList<ReadingDataUnit>) {
        viewModelScope.launch {
            //if (repo.getLocalRdusByEssay(requestId, essayId).isNullOrEmpty()) {
                repo.insertRdus(rduList)
                ghRdus.postValue(rduList as List<ReadingDataUnit>)
                println("insertRdus 01: ${rduList.count()}")
            /*} else {
                repo.updateRdus(rduList)
                ghRdus.postValue(repo.getLocalRdusByEssay(requestId, essayId))
                println("insertRdus 02: ${rduList.count()}")
            }*/
        }
    }

    fun updatePlantSlots(plantSlotList: List<PlantSlot>){
        viewModelScope.launch {
            repo.updatePlantSlots(plantSlotList)
            ghSlots.postValue(plantSlotList)
        }
    }

    fun workPostRduList(rqId: Int, essId: Int, actId: Int){
        println("WorkPostRduList Called!!!")

        val data = Data.Builder()
        data.putInt("rqId", rqId)
        data.putInt("essId", essId)
        data.putInt("actId", actId)

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val request = OneTimeWorkRequestBuilder<PostRduListWorker>()
            //.setConstraints(constraints)                                  // CORREGIR LUEGO !!!!
            .setInputData(data.build())
            .addTag("post-rdulist")
            .setBackoffCriteria(BackoffPolicy.LINEAR, 30, TimeUnit.SECONDS)
            .build()

        WorkManager.getInstance()
            .enqueue(request)
    }

    fun createBioPack(sampleQty: Int, hostQty: Int, insertTypeId: Int): BioPack{
        Log.i("CreateBioPack", "CreateBioPack has been called")

        val agentList = ghAgents.value?.filter { it.essayId == essayId.value }
        val sampList = ghSamples.value
        val plantList = mutableListOf<PlantBySample>()
        val plantSlotList = mutableListOf<PlantSlot>()

        ghActivities.value!!.filter { act -> !(act.activityName.toLowerCase().contains("photo")) }.forEach {
            for (i in 1..sampleQty){
                for (j in 1..hostQty){
                    val sampId = sampList!![i-1].sampleId
                    // TODO Correct this temporary mecanism ordering the registers from Agents table
                    val agentId = when {
                        essayId.value == 3 -> {
                            agentList!![j-1].agentId
                        }
                        j>1 -> {
                            agentList!![j-2].agentId
                        }
                        else -> 131
                    }
                    val plantName = when {
                        essayId.value == 3 -> {
                            agentList!![j-1].agentName
                        }
                        j>1 -> {
                            agentList!![j-2].agentName
                        }
                        else -> "Mother Plant"
                    }
                    val plant = PlantBySample(
                        plantId = j,    //+(hostQty*(i-1)),
                        plantName = plantName, //agentList!![j-1].agentName,
                        sampleId = sampId,
                        sampCorrelative = sampId - (sampList.first().sampleId-1),
                        requestId = requestId.value!!,
                        essayId = essayId.value!!,
                        activityId = it.activityId,
                        insertTypeId = insertTypeId,
                        numOrderId = numOrderId.value!!,
                        cropId = cropId.value!!,
                        workflowId = workflowId.value!!,
                        agentId = agentId
                    )
                    plantList.add(plant)
                }
            }
        }
        return BioPack(ghSamples.value!!, plantList, plantSlotList)
    }

    fun insertSymptoms(){
        viewModelScope.launch {
            repo.insertSymptoms(symptomList)
            ghSymptoms.postValue(symptomList)
        }
    }
}