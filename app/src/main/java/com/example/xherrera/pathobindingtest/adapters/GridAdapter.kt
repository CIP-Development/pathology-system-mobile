package com.example.xherrera.pathobindingtest.adapters


import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.xherrera.pathobindingtest.R
import com.example.xherrera.pathobindingtest.database.entities.Slot
import kotlinx.android.synthetic.main.cell_item.view.*

class GridAdapter(
    private val usedSlots: Int,
    private val lectureList: ArrayList<Slot>,
    private val listener: Listener
): RecyclerView.Adapter<GridAdapter.ViewHolder>() {

    interface Listener {
        fun onItemClick(slot: Slot, position: Int){
        }
    }

    private val colors: Array<String> = arrayOf(
        "#D1C4E9",
        "#C8E6C9",
        "#80D8FF",
        "#FFCDD2",  //LIGHT RED
        "#CCFF90",
        "#B2DFDB",
        "#A7FFEB",
        "#C5CAE9",
        "#90CAF9",  //ORANGE -> LIGHT BLUE
        "#E1BEE7",
        "#80DEEA",
        "#69F0AE",
        "#EA80FC"
    )


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lectureList[position], listener, colors)
        /*
        if (position < usedSlots) {
            holder.itemView.isEnabled = false
            holder.itemView.isFocusable = false
            holder.itemView.isFocusableInTouchMode = false
            holder.itemView.setBackgroundColor(Color.WHITE)
            holder.itemView.imgDiagnosis.setImageResource(R.drawable.ic_empty)
        }
        */
    }


    override fun getItemCount(): Int {
        return lectureList.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_item, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bind(
            slot: Slot,
            listener: Listener,
            colors: Array<String>
        ){
            itemView.setOnClickListener { listener.onItemClick(slot, adapterPosition) }
            itemView.tvCell.text = slot.cellPosition.toString()
            if (slot.type == "control") itemView.cvCell.setCardBackgroundColor(Color.parseColor("#FFF59D"))
            if (slot.type == "blind") itemView.cvCell.setCardBackgroundColor(Color.parseColor("#9E9E9E"))
            if (slot.type == "dBlind") itemView.cvCell.setCardBackgroundColor(Color.parseColor("#9E9E9E"))
            if (slot.type == "problem") itemView.cvCell.setCardBackgroundColor(Color.parseColor("#AD1457"))
            if (slot.type == "10%") itemView.cvCell.setCardBackgroundColor(Color.parseColor("#FF8A65"))
            if (slot.type == "sample") itemView.cvCell.setCardBackgroundColor(Color.parseColor(colors[slot.agentId%colors.count()]))
            if (slot.type == "empty") {
                slot.result = "empty"
                itemView.cvCell.setCardBackgroundColor(Color.WHITE)
                itemView.cvCell.isFocusable = false
                itemView.cvCell.isEnabled = false
                itemView.cvCell.isFocusableInTouchMode = false
                itemView.imgDiagnosis.setImageResource(R.drawable.ic_empty)
            }
            else if (slot.result == "positive") itemView.imgDiagnosis.setImageResource(R.drawable.ic_positive)
            else if (slot.result == "negative") itemView.imgDiagnosis.setImageResource(R.drawable.ic_negative)
            else if (slot.result == "doubt") itemView.imgDiagnosis.setImageResource(R.drawable.ic_doubt)
            else if (slot.result == "buffer") itemView.imgDiagnosis.setImageResource(R.drawable.ic_buffer_prev)
        }
    }
}