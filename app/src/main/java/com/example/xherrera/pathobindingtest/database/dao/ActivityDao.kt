package com.example.xherrera.pathobindingtest.database.dao

import androidx.room.*
import com.example.xherrera.pathobindingtest.database.entities.Activity

@Dao
interface ActivityDao {

    @Query("SELECT * FROM Activity")
    suspend fun getActivityList(): List<Activity>

    @Query("SELECT * FROM Activity WHERE essayId = :essayId")
    suspend fun getActivitiesByEssay(essayId: Int): List<Activity>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(activity: Activity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(activity: Activity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(activityList: List<Activity>)

    @Query("DELETE FROM Activity")
    fun deleteAll()
}